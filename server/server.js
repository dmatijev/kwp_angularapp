'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use(express.static('./'));

 
app.get('*', function(req, res) {
    console.log('404 error', req.path);
    res.sendStatus(404);
});


app.listen(3000, function(){
    console.log("Listening on port 3000 ... ");
});